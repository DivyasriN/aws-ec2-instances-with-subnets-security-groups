from aws_cdk import (
    aws_ec2 as ec2,
    aws_ssm as ssm, Stack,
    aws_iam as iam
)
from constructs import Construct

class MyCdkProjectHw3Stack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here

        env_name = self.node.try_get_context("env")

        #VPC and Subnets Code

        vpc = ec2.Vpc(self, 'Homework3_AWS_VPC',
            max_azs = 2,
            enable_dns_hostnames = True,
            enable_dns_support = True, 
            subnet_configuration=[
                ec2.SubnetConfiguration(
                    name = 'Public-Subnet',
                    subnet_type = ec2.SubnetType.PUBLIC,
                    cidr_mask = 26
                ),
                ec2.SubnetConfiguration(
                    name = 'Private-Subnet',
                    subnet_type = ec2.SubnetType.PRIVATE_WITH_NAT,
                    cidr_mask = 26
                )
            ],
            nat_gateways = 1,

        )
        
        #Code for creating the Security Groups

        sg = ec2.SecurityGroup(
            self,
            id="sg_1",
            vpc=vpc,
            allow_all_outbound=True,
            description="CDK Security Group"
        )
        sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(22),
            description="Allow SSH Access from anywhere",
        )


        sg2 = ec2.SecurityGroup(
            self,
            id="sg_2",
            vpc=vpc,
            allow_all_outbound=True,
            description="CDK Security Group to allow access from SG"
        )
        sg2.add_ingress_rule(
            peer=sg,
            connection=ec2.Port.tcp(22),
            description="Allow SSH Access only from Security group 1",
        )

        #Code for AMI

        amzn_linux = ec2.MachineImage.latest_amazon_linux(
            generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
            edition=ec2.AmazonLinuxEdition.STANDARD,
            virtualization=ec2.AmazonLinuxVirt.HVM,
            storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
            )

        #Code for creating IAM role
        
        role = iam.Role(self, "InstanceSSM", assumed_by=iam.ServicePrincipal("ec2.amazonaws.com"))

        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonSSMManagedInstanceCore"))

        #Code for creating Instance 1

        instance1 = ec2.Instance(self, "Instance1",
            instance_type=ec2.InstanceType("t2.micro"),
            machine_image=amzn_linux,
            vpc = vpc,
            role = role,
            vpc_subnets=ec2.SubnetSelection(
                subnet_type=ec2.SubnetType.PUBLIC
            ),
            security_group=sg,
            key_name="simple-pair-1"
        )

        instance2 = ec2.Instance(self, "Instance2",
            instance_type=ec2.InstanceType("t2.micro"),
            machine_image=amzn_linux,
            vpc = vpc,
            role = role,
            vpc_subnets=ec2.SubnetSelection(
                subnet_type=ec2.SubnetType.PRIVATE_WITH_NAT
            ),
            security_group=sg2,
            key_name="simple-pair-1"
        )
import logging
import sys
from datetime import time
from idlelib import testing

import boto3

from awscli.customizations.dlm import iam
from self import self

from sourcefiles.ssm import region, ssm_client

ec2 = boto3.resource('ec2')
ec2_client = boto3.client('ec2')

ec2 = boto3.resource('ec2', region_name="us-east-1")
ec2_client = boto3.client('ec2', region_name="us-east-1")
# create vpc
sg_vpc = ec2.create_vpc(CidrBlock='10.10.0.0/16')

# tag vpc
sg_vpc.create_tags(Tags=[{"Key": "Name", "Value": "homework3_vpc"}])

# create 1xPublic Subnet & 2xPrivate Subnets
public_subnet1 = sg_vpc.create_subnet(CidrBlock='10.10.1.0/24', AvailabilityZone='us-east-1a')
public_subnet1.meta.client.modify_subnet_attribute(SubnetId=public_subnet1.id, MapPublicIpOnLaunch={"Value": True})

public_subnet2 = sg_vpc.create_subnet(CidrBlock='10.10.2.0/24', AvailabilityZone='us-east-1a')
public_subnet2.meta.client.modify_subnet_attribute(SubnetId=public_subnet2.id, MapPublicIpOnLaunch={"Value": True})
# 2 private subnets
private_subnet1 = sg_vpc.create_subnet(CidrBlock='10.10.3.0/24', AvailabilityZone='us-east-1a')
private_subnet2 = sg_vpc.create_subnet(CidrBlock='10.10.4.0/24', AvailabilityZone='us-east-1a')

# create tag for subnets
public_subnet1_tag = [public_subnet1.id]
public_subnet2_tag = [public_subnet2.id]
private_subnet1_tag = [private_subnet1.id]
private_subnet2_tag = [private_subnet2.id]

ec2_client.create_tags(Resources=public_subnet1_tag, Tags=[{'Key': 'Name', 'Value': 'public_subnet_1'}])
ec2_client.create_tags(Resources=public_subnet2_tag, Tags=[{'Key': 'Name', 'Value': 'public_subnet_2'}])
ec2_client.create_tags(Resources=private_subnet1_tag, Tags=[{'Key': 'Name', 'Value': 'private_subnet_1'}])
ec2_client.create_tags(Resources=private_subnet2_tag, Tags=[{'Key': 'Name', 'Value': 'private_subnet_2'}])

# create Internet Gateway and attach to vpc
igw = ec2.create_internet_gateway()
igw.attach_to_vpc(VpcId=sg_vpc.id)

# Query RouteTableId for public subnet1
public_route_table = list(sg_vpc.route_tables.all())[0]
# add a default route, for Public Subnet, pointing to Internet Gateway
ec2_client.create_route(RouteTableId=public_route_table.id, DestinationCidrBlock='0.0.0.0/0', GatewayId=igw.id)
public_route_table.associate_with_subnet(SubnetId=public_subnet1.id)

# Query RouteTableId  for public subnet2
public_route_table.associate_with_subnet(SubnetId=public_subnet2.id)

# create a route table for 2xPrivate Subnets
private_route_table = ec2.create_route_table(VpcId=sg_vpc.id)
private_route_table.associate_with_subnet(SubnetId=private_subnet1.id)
private_route_table.associate_with_subnet(SubnetId=private_subnet2.id)

# 6) =============Create a security group 1 and allow SSH inbound rule through the VPC
# create a security group called "public_sg" for Public Subnet
security_group1 = ec2.create_security_group(GroupName='SecGroup1-SSH-ONLY', Description='only allow SSH traffic', VpcId=sg_vpc.id)
security_group1.authorize_ingress(IpProtocol="tcp", CidrIp="0.0.0.0/0", FromPort=22, ToPort=22)
security_group1.authorize_ingress(IpProtocol="tcp", CidrIp="0.0.0.0/0", FromPort=80, ToPort=80)
security_group1.authorize_ingress(IpProtocol="tcp", CidrIp="0.0.0.0/0", FromPort=443, ToPort=443)

# create a file to store the key locally
outfile = open('hw3-ec2-keypair.pem', 'w')
#
# # call the boto ec2 function to create a key pair
key_pair = ec2.create_key_pair(KeyName='hw3-ec2-keypair')
#
# # capture the key and store it in a file
KeyPairOut = str(key_pair.key_material)
outfile.write(KeyPairOut)

# create a security group called "private_security_group2" for Private Subnet
security_group2 = ec2.create_security_group(GroupName='private_security_group2', Description='private_sg', VpcId=sg_vpc.id)
# Allow All Traffic from Public Subnet 10.10.1.0/24 to Private Subnet
security_group2.authorize_ingress(CidrIp='10.10.1.0/24', IpProtocol='-1', FromPort=-1, ToPort=-1)

# Allow All Traffic between 2 x Private Subnets
security_group2.authorize_ingress(CidrIp='10.10.2.0/24', IpProtocol='-1', FromPort=-1, ToPort=-1)
security_group2.authorize_ingress(CidrIp='10.10.3.0/24', IpProtocol='-1', FromPort=-1, ToPort=-1)
# # Allow All Traffic from Private Subnet 10.10.2.0/24 & 10.10.2.0/24
security_group1.authorize_ingress(CidrIp='10.10.2.0/24', IpProtocol='-1', FromPort=-1, ToPort=-1)
security_group1.authorize_ingress(CidrIp='10.10.3.0/24', IpProtocol='-1', FromPort=-1, ToPort=-1)


# Launch an EC2 Instance in public subnet
public_instance_1 = public_subnet1.create_instances(
    ImageId='ami-090fa75af13c156b4',
    MinCount=1,
    MaxCount=1,
    KeyName='hw3-ec2-keypair',
    InstanceType='t2.micro',
    SecurityGroupIds=[security_group1.id])

# #  Launch an EC2 Instance in Private Subnet
private_instance_1 = private_subnet1.create_instances(
    ImageId='ami-090fa75af13c156b4',
    MinCount=1,
    MaxCount=1,
    KeyName='hw3-ec2-keypair',
    SecurityGroupIds=[security_group2.id],
    InstanceType='t2.micro')
# Code for AMI

amazon_linux = ec2.MachineImage.latest_amazon_linux(
    generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
    edition=ec2.AmazonLinuxEdition.STANDARD,
    virtualization=ec2.AmazonLinuxVirt.HVM,
    storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
)

# Code for creating IAM role

role = iam.Role(self, "InstanceSSM", assumed_by=iam.ServicePrincipal("ec2.amazonaws.com"))

role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonSSMManagedInstanceCore"))

# logger
def setup_logger(region):
    data = {'region': region}

    logger = logging.getLogger()
    for handler in logger.handlers:
        logger.removeHandler(handler)
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] [%(region)s] : %(message)s')
    handler.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger = logging.LoggerAdapter(logger, data)
    return logger


# initialize the logger
logger = setup_logger(region)
instances = boto3.client.describe_instances(Filters=[
    {
        'Name': 'instance-state-name',
        'Values': ['running']
    }
])

running_instance = []

if len(instances['Reservations']) == 0:
    logger.info(f"No Instances found!")
    exit()

for instance in instances['Reservations']:
    for i in instance['Instances']:
        running_instance.append(i['InstanceId'])

if testing == "True":
    logger.info(f"Executing commands on the following instances: {','.join(running_instance)}")
else:
    try:
        logger.warning(f"Executing commands on the following instances:  {','.join(running_instance)}")
        response = ssm_client.send_command(
            InstanceIds=[r for r in running_instance],
            DocumentName="AWS-RunShellScript",
            Parameters={'commands': ["sudo yum update -y",
                                     "echo 'File Created by SSM agent!' > text.txt"]}, )
        command_id = response['Command']['CommandId']
        logger.info(f"Command id: {command_id}")
        for instance in running_instance:
            command_invocation_result = ssm_client.get_command_invocation(CommandId=command_id, InstanceId=instance)
            time.sleep(5)
            if command_invocation_result['ResponseCode'] == -1:
                logger.info(f"{command_invocation_result['Status']}")
                time.sleep(5)
            logger.info(f"{command_invocation_result['Status']}")

    except Exception as e:
        logger.error(
            f"Failed to execute commands on the following instances:{','.join(running_instance)} with error {e}!")
